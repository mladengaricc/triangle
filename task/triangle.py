def is_triangle(a, b, c):
    # Lets check if the sum of the lengths of any two sides is greater than the length of the third side
    # That is a valid condition for a triangle with a nonzero area
    if a>0 and b>0 and c>0 and a+b>c and a+c>b and b+c>a:
        return True
    else:
        return False
